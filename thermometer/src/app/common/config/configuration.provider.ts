import { Injectable } from '@angular/core';

enum Environments {
    LOCAL,
    DEVELOPMENT,
    PRODUCTION
}

const CURRENT_ENVIRONMENT: Environments = Environments.LOCAL;

@Injectable()
export class ConfigurationProvider {
    constructor() { }

    get environmentVariable(): string {
        if (CURRENT_ENVIRONMENT === Environments.PRODUCTION) {
            return 'PROD';
        } else if (CURRENT_ENVIRONMENT === Environments.LOCAL) {
            return 'LOCAL';
        } else {
            return 'DEV';
        }
    }

    get apiBaseUrl(): string {
        if (CURRENT_ENVIRONMENT === Environments.PRODUCTION) {
            return '';
        } else if (CURRENT_ENVIRONMENT === Environments.LOCAL) {
            // return 'http://localhost:5050/services/api/v1/thermo/';
            //return 'http://g43wrfb2e.logon.ds.ge.com:5050/services/api/v1/thermo/'
            return 'http://g43wrfb2e.logon.ds.ge.com:5050/services/api/v1/'
        } else {
            return '';
        }
    }
}
