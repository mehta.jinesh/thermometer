import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { OnboardingService } from '../services/onboarding.service';

@Injectable()
export class CreateOnBoardingDetailsResolver implements Resolve<any> {

    constructor(private onBoardingService: OnboardingService) { }

    resolve(): Observable<any> {
        const responseOK = this.onBoardingService.getMetaData();
        return responseOK.pipe(take(1));
    }
}
