import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { OnboardingService } from '../services/onboarding.service';

@Injectable()
export class ViewOnBoardingDetailsResolver implements Resolve<any> {

    constructor(private onBoardingService: OnboardingService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        let params: any = {};
        route.pathFromRoot.forEach((path) => {
            if (Object.keys(path.params).length) {
                params = { ...params, ...path.params };
            }
        });
        const responseOK = this.onBoardingService.getOnBoardingProjectDetails(params.id);
        return responseOK.pipe(take(1));
    }
}
