export const DISPLAYED_COLUMN = ['tollgateName', 'appName', 'criticality',
    'isDelayed', 'geAppOwner', 'itLeader', 'currentVendor', 'plannedStartDate', 'ktStartDate', 'primaryStartDate',
    'ktStatus', 'risk', 'riskDesc', 'mitigation'];