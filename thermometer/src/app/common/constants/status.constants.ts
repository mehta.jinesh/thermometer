export const TOLLGATE_STATUS = [
    { key: 'NOTSTARTED', value: 'NOT STARTED' },
    { key: 'INPROGRESS', value: 'IN PROGRESS' },
    { key: 'COMPLETED', value: 'COMPLETED' },
];