export const CRITICAL_LEVELS = [
    { key: 'TIER1', value: 'TIER 1' },
    { key: 'TIER2', value: 'TIER 2' },
    { key: 'TIER3', value: 'TIER 3' },
    { key: 'TIER4', value: 'TIER 4' }
];