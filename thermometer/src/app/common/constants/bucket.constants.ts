export const BUCKET_LIST = [
    {
      "id": 1,
      "name": "Services & Risk"
    },
    {
      "id": 2,
      "name": "Digital Engineering"
    },
    {
      "id": 3,
      "name": "GPS"
    },
    {
      "id": 4,
      "name": "Supply Chain"
    }
  ];