import { TestBed } from '@angular/core/testing';

import { PrimaryPhasedataService } from './primary-phasedata.service';

describe('PrimaryPhasedataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrimaryPhasedataService = TestBed.get(PrimaryPhasedataService);
    expect(service).toBeTruthy();
  });
});
