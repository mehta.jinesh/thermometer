import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { OnboardingDetails } from '../models/onboarding-details';
import { ConfigurationProvider } from '../config/configuration.provider';
import { MODULE } from '../constants/module.constants';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class OnboardingService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private http: HttpClient,
    private config: ConfigurationProvider) { }

  addOnBoardingDetails(onBoardingDetails: OnboardingDetails): Observable<OnboardingDetails> {
    return this.http.post<OnboardingDetails>(`${this.config.apiBaseUrl}/${MODULE.THERMO}/saveOnBoarding`,
      onBoardingDetails, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );

  }

  saveTollGateDetails(saveTollGateDetails: any): Observable<any> {
    return this.http.post<any>(`${this.config.apiBaseUrl}/${MODULE.THERMO}/saveTollGateDetails`,
      saveTollGateDetails, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );

  }

  getOnBoardingProjectDetails(id: number): Observable<any> {
    return this.http.get<any>(`${this.config.apiBaseUrl}/${MODULE.THERMO}/getProject/${id}`, httpOptions)
      .pipe(tap(res => {
        return res;
      }),
        catchError(this.handleError)
      );

  }

  getOnboardingDetails(): Observable<any> {
    return this.http.get<any>(`${this.config.apiBaseUrl}/${MODULE.THERMO}/getAllProjects`, httpOptions)
      .pipe(tap(res => {
        return res;
      }),
        catchError(this.handleError)
      );
  }

  getCIOList(): Observable<any> {
    return this.http.get<any>(`${this.config.apiBaseUrl}/${MODULE.CIO}/getAllCio`, httpOptions)
      .pipe(tap(res => {
        return res;
      }),
        catchError(this.handleError)
      );
  }

  getMetaData(): Observable<any> {
    return this.http.get<any>(`${this.config.apiBaseUrl}/${MODULE.THERMO}/getMetaData`, httpOptions)
      .pipe(
        tap(res => {
          return res;
        }),
        catchError(this.handleError)
      );
  }

  /**
   * Handling service error
   * @param error error
   */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}
