import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ConfigurationProvider } from '../config/configuration.provider';
import { catchError, tap } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

import { NotificationsService } from 'angular2-notifications';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
import { MODULE } from '../constants/module.constants';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient,
    private config: ConfigurationProvider,
    private notificationsService: NotificationsService) { }

  // Get the list of CIO's
  getCIOList(): Observable<any> {
    return this.http.get<any>(`${this.config.apiBaseUrl}/${MODULE.CIO}/getAllCio/`, httpOptions)
      .pipe(tap(res => {
        return res;
      }),
        catchError(this.handleError)
      );
  }

  // Get stack chart (count of projects per tollgate) data based on CIO Id
  getTollgateChartData(id: number): Observable<any> {
    return this.http.get<any>(`${this.config.apiBaseUrl}/${MODULE.DASHBOARD}/getCountByStatusAndCio/${id}`, httpOptions)
      .pipe(tap(res => {
        return res;
      }),
        catchError(this.handleError)
      );
  }

  // Get stack chart (count of projects per tollgate) data for ALL CIO's
  getAllTollgateChartData(): Observable<any> {
    return this.http.get<any>(`${this.config.apiBaseUrl}/${MODULE.DASHBOARD}/getProjectCountByTollgate`, httpOptions)
      .pipe(tap(res => {
        return res;
      }),
        catchError(this.handleError)
      );
  }

   // Get the list of CIO's project data
   getOnboardingsData(id: number): Observable<any> {
    return this.http.get<any>(`${this.config.apiBaseUrl}/${MODULE.DASHBOARD}/getProjectByCio/${id}`, httpOptions)
      .pipe(tap(res => {
        return res;
      }),
        catchError(this.handleError)
      );
  }

   // Get the list of ALL project data
   getAllOnboardingsData(id: number): Observable<any> {
    return this.http.get<any>(`${this.config.apiBaseUrl}/${MODULE.DASHBOARD}/getAllProjectInfo`, httpOptions)
      .pipe(tap(res => {
        return res;
      }),
        catchError(this.handleError)
      );
  }

  /**
 * Handling service error
 * @param error error
 */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
