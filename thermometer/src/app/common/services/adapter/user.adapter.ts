
import { User } from '../../models/user';

export class UserAdapter {
    static parseAPIResponse(data: any) {
        return new User({
            userId: data.id,
            userName: data.id,
            firstName: data.id,
            lastName: data.id,
            address: data.id,
            contactNumber: data.id,
            email: data.id,
            status: data.id,
        });
    }
}
