import { TollGateMaster } from '../../models/tollgate-master';
import { CriteriaMasterAdapter } from './criteria-master.adapter';

export class TollGateMasterAdapter {
    static parseAPIResponse(data: any) {
        return new TollGateMaster({
            id: data.id,
            tollgateLevel: data.tollgateLevel,
            criteriaQuestionDetails : data.criteriaQuestionDetails.map(r => {
                return CriteriaMasterAdapter.parseAPIResponse(r);
            }),
        });
    }
}
