import { OnboardingDetails } from '../../models/onboarding-details';
import { TollGateMasterAdapter } from './tollgate-master.adapter';


export class OnBoardingAdapter {
    static parseAPIResponse(data: any) {
        return new OnboardingDetails({
            id: data.id,
            projectName: data.projectName,
            applicationName: data.applicationName,
            geItLeader: data.geItLeader,
            geFunctionalLeader: data.geFunctionalLeader,
            pitcher: data.pitcher,
            tcsPm: data.tcsPm,
            geProjectOwner: data.geProjectOwner,
            catcher: data.catcher,
            ktOwner: data.ktOwner,
            subBusiness: data.subBusiness,
            subBusinessLead: data.subBusinessLead,
            exportControl: data.exportControl,
            monitoring: data.monitoring,
            functionalSupport: data.functionalSupport,
            functionalMonitor: data.functionalMonitor,
            onsiteSupport: data.onsiteSupport,
            admin: data.admin,
            enhancement: data.enhancement,
            tollGateDetailsResponse: data.tollGateDetailsResponse.map((eachh) =>
                TollGateMasterAdapter.parseAPIResponse(eachh))
        });
    }
}
