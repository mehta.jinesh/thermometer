import { CriteriaMaster } from '../../models/criteria-master';
import { QuestionMasterAdapter } from './question-master.adapter';

export class CriteriaMasterAdapter {
    static parseAPIResponse(data: any) {
        return new CriteriaMaster({
            id: data.id,
            criteriaDesc: data.criteriaDesc,
             questionDetails: data.questionDetails.map(r => {
                return QuestionMasterAdapter.parseAPIResponse(r);
            }),
        });
    }
}
