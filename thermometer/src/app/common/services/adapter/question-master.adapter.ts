import { QuestionMaster } from '../../models/question-master';

export class QuestionMasterAdapter {
    static parseAPIResponse(data: any) {
        return new QuestionMaster({
            id: data.id,
            question: data.question
        });
    }
}

