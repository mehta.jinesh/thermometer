import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

    statusList: any[];
    statusSelected: any;

    constructor(
        public dialogRef: MatDialogRef<ModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit() {
        console.log(this.data);

        if (this.data && this.data.risk) {
            if (this.data.risk !== 'NONE') {
                if (this.data.status && this.data.status !== 'NOTSTARTED') {
                    if (this.data.status === 'INPROGRESS' || this.data.status === 'RISK') {
                        this.statusList = [
                            { key: 'INPROGRESS', value: 'IN PROGRESS' },
                            { key: 'RISK', value: 'AT RISK' }
                        ];
                    } else if (this.data.status === 'COMPLETED') {
                        this.statusList = [
                            { key: 'COMPLETED', value: 'COMPLETED' }
                        ];
                    }
                } else {
                    this.statusList = [
                        { key: 'NOTSTARTED', value: 'NOT STARTED' },
                        { key: 'INPROGRESS', value: 'IN PROGRESS' },
                        { key: 'RISK', value: 'AT RISK' }
                    ];
                }
            } else {
                if (this.data.status && this.data.status !== 'NOTSTARTED') {
                    if (this.data.status === 'INPROGRESS') {
                        this.statusList = [
                            { key: 'INPROGRESS', value: 'IN PROGRESS' },
                            { key: 'COMPLETED', value: 'COMPLETED' }
                        ];
                    } else if (this.data.status === 'COMPLETED') {
                        this.statusList = [
                            { key: 'COMPLETED', value: 'COMPLETED' }
                        ];
                    }
                } else {
                    this.statusList = [
                        { key: 'NOTSTARTED', value: 'NOT STARTED' },
                        { key: 'INPROGRESS', value: 'IN PROGRESS' },
                        { key: 'COMPLETED', value: 'COMPLETED' }
                    ];
                }
            }
        }

        // setting to by default option
        this.statusSelected = this.data && this.data.status ? this.data.status : 'NOTSTARTED';
    }

}