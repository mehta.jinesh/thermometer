import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class NavbarComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.router.events.subscribe((res) => {
      if (res instanceof NavigationEnd) {
        const navLinks = document.querySelectorAll('.mat-tab-links .mat-tab-link');
        navLinks.forEach(link => {
          res.url.includes(link.getAttribute('href')) ? link.classList.add('active') : link.classList.remove('active');
        });
      }
    });
  }

}
