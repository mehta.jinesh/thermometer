import { TollGateMaster } from './tollgate-master';

export interface IOnboardingDetails {
    id: number;
    projectName: string;
    applicationName: string;
    geItLeader: string;
    geFunctionalLeader: string;
    pitcher: string;
    tcsPm: string;
    geProjectOwner: string;
    catcher: string;
    ktOwner: string;
    subBusiness: string;
    subBusinessLead: string;
    exportControl: boolean;
    monitoring: boolean;
    functionalSupport: boolean;
    functionalMonitor: boolean;
    onsiteSupport: boolean;
    admin: boolean;
    enhancement: boolean;
    tollGateDetailsResponse: TollGateMaster[];
}

export class OnboardingDetails {
    id = null;
    projectName = '';
    applicationName = '';
    geItLeader = '';
    geFunctionalLeader = '';
    pitcher = '';
    tcsPm = '';
    geProjectOwner = '';
    catcher = '';
    ktOwner = '';
    subBusiness = '';
    subBusinessLead = '';
    exportControl = null;
    monitoring = null;
    functionalSupport = null;
    functionalMonitor = null;
    onsiteSupport = null;
    admin = null;
    enhancement = null;
    tollGateDetailsResponse: TollGateMaster[] = null;

    constructor(data: IOnboardingDetails) {
        this .id = data.id;
        this .projectName = data.projectName;
        this .applicationName = data.applicationName;
        this .geItLeader = data.geItLeader;
        this .geFunctionalLeader = data.geFunctionalLeader;
        this .pitcher = data.pitcher;
        this .tcsPm = data.tcsPm;
        this .geProjectOwner = data.geProjectOwner;
        this .catcher = data.catcher;
        this .ktOwner = data.ktOwner;
        this .subBusiness = data.subBusiness;
        this .subBusinessLead = data.subBusinessLead;
        this .exportControl = data.exportControl;
        this .monitoring = data.monitoring;
        this .functionalSupport = data.functionalSupport;
        this .functionalMonitor = data.functionalMonitor;
        this .onsiteSupport = data.onsiteSupport;
        this .admin = data.admin;
        this .enhancement = data.enhancement;
        this.tollGateDetailsResponse = data.tollGateDetailsResponse;
    }
}
