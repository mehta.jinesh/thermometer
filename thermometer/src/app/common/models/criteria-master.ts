import { QuestionMaster } from './question-master';


export interface ICriteriaMaster {
    id: number;
    criteriaDesc: string;
    questionDetails: QuestionMaster[];
}
export class CriteriaMaster {
    id = null;
    criteriaDesc = '';
    questionDetails = null;

    constructor(data: ICriteriaMaster) {
        this .id = data.id;
        this .criteriaDesc = data.criteriaDesc;
        this .questionDetails = data.questionDetails;
    }
}
