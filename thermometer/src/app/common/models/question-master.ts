

export interface IQuestionMaster {
    id: number;
    question: string;

}
export class QuestionMaster {
    id = null;
    question = '';

    constructor(data: IQuestionMaster) {
        this .id = data.id;
        this .question = data.question;
    }
}
