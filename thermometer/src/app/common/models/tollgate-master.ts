import { CriteriaMaster } from './criteria-master';

export interface ITollGateMaster {
    id: number;
    tollgateLevel: string;
    criteriaQuestionDetails: CriteriaMaster[];
}
export class TollGateMaster {
    id = null;
    tollgateLevel = '';
    criteriaQuestionDetails = null;

    constructor(data: ITollGateMaster) {
        this .id = data.id;
        this .tollgateLevel = data.tollgateLevel;
        this .criteriaQuestionDetails = data.criteriaQuestionDetails;

    }
}
