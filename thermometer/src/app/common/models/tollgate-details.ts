import { TollgateResponse } from './tollgate-response'

export interface ISaveTollGateDetails {
    projectId: number;
    tollGateId: number;
    tollgateResponse: TollgateResponse[];
}

export class SaveTollGateDetails {
    projectId = null;
    tollGateId = null;
    saveTollGateDetails = null;
    tollgateResponse = null;
    constructor(data: ISaveTollGateDetails) {
        this.projectId = data.projectId;
        this.tollGateId = data.tollGateId;
        this.tollgateResponse = data.tollgateResponse;
    }

}




