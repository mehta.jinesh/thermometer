export interface ITollgateResponse {
    actions: string;
    value: boolean;
    remarks: string;
    questionId: number;
}
export class TollgateResponse {

    actions = '';
    value = null;
    remarks = '';
    questionId = null;

    constructor(data: ITollgateResponse) {
        this.actions = data.actions;
        this.value = data.value;
        this.remarks = data.remarks;
        this.questionId = data.questionId;
    }
}