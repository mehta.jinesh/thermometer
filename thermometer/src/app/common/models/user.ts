
export interface IUser {
    userId: number;
    userName: string;
    firstName: string;
    lastName: string;
    address: string;
    contactNumber: string;
    email: string;
    status: string;
}

export class User {
    userId = null;
    userName = '';
    firstName = '';
    lastName = '';
    address = '';
    contactNumber = '';
    email = '';
    status = '';

    constructor(data: IUser) {
        this .userId = data.userId;
        this .userName = data.userName;
        this .firstName = data.firstName;
        this .lastName = data.lastName;
        this .address = data.address;
        this .contactNumber = data.contactNumber;
        this .email = data.email;
        this .status = data.status;
    }
}
