export interface QuestionDetail {
    id: number;
    question: string;
}

export interface CriteriaQuestionDetail {
    id: number;
    criteriaDesc: string;
    questionDetails: QuestionDetail[];
    ktThermometerPercentage: number;
}

export interface TollGate {
    id: number;
    tollgateLevel: string;
    criteriaQuestionDetails: CriteriaQuestionDetail[];
    risk: string;
    riskDesc: string;
    status: string;
    mitigation: string;
}
