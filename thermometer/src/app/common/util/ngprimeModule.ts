import { NgModule } from '@angular/core';

import {DataTableModule} from 'primeng/datatable';

@NgModule({
    imports: [
        DataTableModule
        
    ],
    exports: [
        DataTableModule
    ]
})
export class NgprimeModule {}
