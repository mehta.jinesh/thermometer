import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatInputModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { MatIconModule } from '@angular/material';
import { MatSelectModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material';
import { MatTooltipModule } from '@angular/material';
import { MatCardModule } from '@angular/material';
import { MatTabsModule } from '@angular/material';
import { MatSortModule } from '@angular/material';
import { MatPaginatorModule } from '@angular/material';
import { MatNativeDateModule } from '@angular/material';
import { MatDialogModule } from '@angular/material';
import { MatMenuModule } from '@angular/material';
import { MatButtonModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material';
import { MatSnackBarModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material';
import { MatListModule } from '@angular/material';
import { MatChipsModule } from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatRadioModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatBadgeModule} from '@angular/material/badge';

@NgModule({
    imports: [
        MatInputModule,
        MatTableModule,
        MatMenuModule,
        MatIconModule,
        MatStepperModule,
        MatSelectModule,
        MatButtonModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatTooltipModule,
        MatCardModule,
        MatTabsModule,
        MatSortModule,
        MatPaginatorModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatDialogModule,
        MatMenuModule,
        MatButtonModule,
        MatExpansionModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        MatListModule,
        MatChipsModule,
        MatCheckboxModule,
        MatAutocompleteModule,
        MatSlideToggleModule,
        MatRadioModule,
        MatGridListModule,
        MatProgressBarModule,
        MatBadgeModule
    ],
    exports: [
        MatInputModule,
        MatTableModule,
        MatMenuModule,
        MatIconModule,
        MatFormFieldModule,
        MatSelectModule,
        MatStepperModule,
        MatButtonModule,
        MatToolbarModule,
        MatTooltipModule,
        MatCardModule,
        MatTabsModule,
        MatSortModule,
        MatPaginatorModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatDialogModule,
        MatMenuModule,
        MatButtonModule,
        MatExpansionModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        MatListModule,
        MatChipsModule,
        MatCheckboxModule,
        MatAutocompleteModule,
        MatSlideToggleModule,
        MatRadioModule,
        MatGridListModule,
        MatProgressBarModule,
        MatBadgeModule
    ]
})
export class MaterialModule { }
