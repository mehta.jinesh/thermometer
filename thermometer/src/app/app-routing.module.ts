// angular module
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// commom components
import { HomePageComponent } from './home-page/home-page.component';

// page components
import { NewOnboardingComponent } from './new-onboarding/new-onboarding.component';
import { ViewOnboardingComponent } from './view-onboarding/view-onboarding.component';
import { DashboardComponent } from './dashboard/dashboard.component';

// resolvers
import { CIOListResolver } from './common/resolvers/cioList.resolver';
import { ViewOnBoardingListResolver } from './common/resolvers/viewOnBoardingList.resolver';
import { ViewOnBoardingDetailsResolver } from './common/resolvers/viewOnBoardingDetails.resolver';
import { CreateOnBoardingDetailsResolver } from './common/resolvers/createOnBoardingDetails.resolver';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomePageComponent,
    data: { title: 'Home' }
  },
  {
    path: 'create-onboarding',
    component: NewOnboardingComponent,
    data: { title: 'NewOnboarding' },
    resolve: {
      cioList: CIOListResolver,
      createOnBoardingDetails: CreateOnBoardingDetailsResolver
    }
  },
  {
    path: 'view-onboarding',
    component: ViewOnboardingComponent,
    data: { title: 'Nav' },
    resolve: {
      viewOnBoardingList: ViewOnBoardingListResolver
    }
  },
  {
    path: 'view-onboarding/:id',
    component: NewOnboardingComponent,
    data: { title: 'NewOnboarding' },
    resolve: {
      cioList: CIOListResolver,
      viewOnBoardingDetails: ViewOnBoardingDetailsResolver
    }
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: { title: 'Dashboard' }
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
