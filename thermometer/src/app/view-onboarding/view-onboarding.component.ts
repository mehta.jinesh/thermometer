import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-onboarding',
  templateUrl: './view-onboarding.component.html',
  styleUrls: ['./view-onboarding.component.scss']
})
export class ViewOnboardingComponent implements OnInit {

  onBoardingDetails: any;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.onBoardingDetails = this.route.snapshot.data.viewOnBoardingList.onBoardingList;
  }

}
