import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewOnboardingComponent } from './view-onboarding.component';

describe('ViewOnboardingComponent', () => {
  let component: ViewOnboardingComponent;
  let fixture: ComponentFixture<ViewOnboardingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewOnboardingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewOnboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
