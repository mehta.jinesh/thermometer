import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms'
import { NotificationsService } from 'angular2-notifications';
import { DashboardService } from '../common/services/dashboard.service';

import { MatTable, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

// themes begin
am4core.useTheme(am4themes_animated);

// Constant
import { BUCKET_LIST } from '../common/constants/bucket.constants';
import { DISPLAYED_COLUMN } from '../common/constants/displaycol.constant';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  cioList: any;
  bucketList: any;
  stackChartData: any;
  filterChar: string = "";
  onboardingsData: any = [];  // Contains the list of all the onboardings
  cioSelect: FormControl;
  bucketSelect: FormControl;
  selectedValue: number = 1;
  selectedBucket: number = 1;
  statusPieChartData: any[];
  tollgatePieChartData: any[];
  riskOnboardings: number = 0;
  totalOnboardings: number = 0;
  completedOnboardings: number = 0;
  inProgressOnboardings: number = 0;
  totalOnboardingsToHold: any = [];
  isDataPresent: boolean = false;
  showMoreDetails: boolean = false;
  noDataToDisplay: boolean = false;


  // Amchart object 
  private chart: am4charts.XYChart;

  // Setting columns to display in the table
  displayedColumns: string[] = DISPLAYED_COLUMN;

  /** ******************** Mat table Pagination & Sort STARTS ********************* */
  @ViewChild('table') table: MatTable<any>;
  @ViewChild('tableAll') tableAll: MatTable<any>;

  private paginator: MatPaginator;
  private sort: MatSort;

  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }
  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }
  setDataSourceAttributes() {
    this.onboardingsData.paginator = this.paginator;
    this.onboardingsData.sort = this.sort;
  }
  /** ********************************* Set up ENDS ********************************** */

  // Component constructor
  constructor(private zone: NgZone,
    private dashboardService: DashboardService,
    private notificationsService: NotificationsService) { }

  // Lifecycle hook
  ngOnInit() {
    this.componentInitialize();
  }

  ngAfterViewInit() { }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  componentInitialize(): void {

    // Get the list of constant buckets
    this.bucketList = BUCKET_LIST;

    // Get list of CIO's
    this.getCIOList();

    // Get CIO level chart data based on tollgates
    this.getTollgateChartData(this.selectedValue);

    // Get project details of the selected project for tabular format
    this.getOnboardingsData(this.selectedValue);

    // Setting flag to display the tabular data
    this.showMoreDetails = true;

    // Checking form data validation
    this.cioSelect = new FormControl('', [Validators.required]);
    this.bucketSelect = new FormControl('', [Validators.required]);
  }

  /** Get the list of all the available CIO's */
  getCIOList(): void {
    // service call
    this.dashboardService.getCIOList()
      .subscribe((response: any) => {
        this.cioList = response;
      });
  }

  /** Get the CIO based tollgate data & plotting the graph based on that */
  getTollgateChartData(selectedValue: number): void {
    // Code to get selected CIO project tollgate data
    if (selectedValue != null && selectedValue > 0) {
      // service call
      this.dashboardService.getTollgateChartData(selectedValue)
        .subscribe((response: any) => {
          this.stackChartData = this.restructureToChartData(response);

          // Plotting the graph based on CIO Id and data
          this.plotStackedGraph();

          // Creating "project status" pie charts
          this.createProjectPie();

          // Creating "tollgate" pie chart
          this.createTollgatePie();
        });
    } else if (selectedValue == 0) { // Code for "All Onboardings" selection
      // service call
      this.dashboardService.getAllTollgateChartData()
        .subscribe((response: any) => {
          this.stackChartData = this.restructureToChartData(response);

          // Plotting the graph based on ALL onboardings
          this.plotStackedGraph();

          // Creating "project status" pie charts
          this.createProjectPie();

          // Creating "tollgate" pie chart
          this.createTollgatePie();
        });
    } else if (selectedValue == -1) { // Code for "OTHERS" selection
    // service call
    this.dashboardService.getTollgateChartData(selectedValue)
      .subscribe((response: any) => {
        this.stackChartData = this.restructureToChartData(response);

        // Plotting the graph based on ALL onboardings
        this.plotStackedGraph();

        // Creating "project status" pie charts
        this.createProjectPie();

        // Creating "tollgate" pie chart
        this.createTollgatePie();
      });
  } else {
      this.showNotificationError("Please select a CIO.");
    }
  }

  /** Method used to call the plotPieChart method sending the required input variables */
  createTollgatePie() {
    var prjStatusColorList = [am4core.color("#B3E5FC"), am4core.color("#4FC3F7"), am4core.color("#03A9F4"),
    am4core.color("#0288D1"), am4core.color("#01579B"), am4core.color("#26C6DA"), am4core.color("#00ACC1")];

    this.plotPieChart(this.tollgatePieChartData, "tgPieChartdiv", "Overall Project Tollgates (%)", prjStatusColorList);
  }

  createProjectPie() {
    var prjStatusColorList = [am4core.color("#E53935"), am4core.color("#F97D09"), am4core.color("#278D27")];
    this.plotPieChart(this.statusPieChartData, "riskPieChartdiv", "Overall Project Status (%)", prjStatusColorList);
  }

  /** Creating the required formatted JSON for plotting both Stack & Pie charts */
  restructureToChartData(response: any[]): any {
    var tempJsonList = [];
    var count: number = 0;

    var countPie: number = 0;
    var tempJsonListPie = [];
    var tempJsonListPieStat = [];

    if (response != null && response.length > 0) {
      for (var i = 0; i < response.length; i++) {

        /** Creating "Tollgate vs Project Count" pie chart data ******* STARTS ******* */
        var tempJsonPie = { "name": "", "count": 0 };

        countPie = response[i].count.COMPLETED + response[i].count.INPROGRESS
          + response[i].count.RISK + response[i].count.NOTSTARTED

        tempJsonPie.name = response[i].tollgateLevel + " (" + response[i].tollgateName + ")";
        tempJsonPie.count = countPie;
        tempJsonListPie.push(tempJsonPie);
        /** Creating pie chart data - *********** ENDS *********** */


        /** Creating stack chart data - *********** STARTS *********** */
        var tempJson = { "TG": "" };
        tempJson.TG = response[i].tollgateLevel + ": " + response[i].tollgateName;
        if (response[i].count.RISK > 0) {
          count++;
          this.riskOnboardings = this.riskOnboardings + response[i].count.RISK;
          tempJson["Risk"] = response[i].count.RISK;
        }
        if (response[i].count.COMPLETED > 0) {
          count++;
          this.completedOnboardings = this.completedOnboardings + response[i].count.COMPLETED;
          tempJson["Completed"] = response[i].count.COMPLETED;
        }
        if (response[i].count.INPROGRESS > 0) {
          count++;
          this.inProgressOnboardings = this.inProgressOnboardings + response[i].count.INPROGRESS;
          tempJson["InProgress"] = response[i].count.INPROGRESS;
        }
        tempJsonList.push(tempJson);
        /** Creating stack chart data - *********** ENDS *********** */
      }
      this.tollgatePieChartData = tempJsonListPie;

      /** Creating "Project Status" pie chart data ******* STARTS ******* */
      var tempJsonPieStat = { "name": "", "count": 0 };
      tempJsonPieStat.name = "Risk";
      tempJsonPieStat.count = this.riskOnboardings;
      tempJsonListPieStat.push(tempJsonPieStat);
      var tempJsonPieStat = { "name": "", "count": 0 };

      tempJsonPieStat.name = "InProgress";
      tempJsonPieStat.count = this.inProgressOnboardings;
      tempJsonListPieStat.push(tempJsonPieStat);
      var tempJsonPieStat = { "name": "", "count": 0 };

      tempJsonPieStat.name = "Completed";
      tempJsonPieStat.count = this.completedOnboardings;
      tempJsonListPieStat.push(tempJsonPieStat);

      this.statusPieChartData = tempJsonListPieStat;
      /** Creating "Project Status" pie chart data ******* ENDS ******* */

      if (count == 0) { this.isDataPresent = true }
    }
    // Getting the total onboardings count
    this.totalOnboardings = this.riskOnboardings + this.completedOnboardings + this.inProgressOnboardings;

    return tempJsonList;
  }

  /** Service call to get CIO project details for tabular format  */
  getOnboardingsData(selectedValue: number): void {
    var tempProjectData = [];
    if (selectedValue != null && selectedValue > 0) {
      // service call
      this.dashboardService.getOnboardingsData(selectedValue)
        .subscribe((response: any) => {
          // Using MatTableDataSource table functions of filter, sort & pagination
          this.onboardingsData = new MatTableDataSource(response[0].projects);

          if (this.onboardingsData != null && this.onboardingsData.data.length == 0) {
            this.noDataToDisplay = true;
            this.showNotificationError('No data found for selected CIO');
          }

          // Holding the entire list of projects in separate list variable
          if (this.onboardingsData != null) {
            this.totalOnboardingsToHold = this.onboardingsData.data;
          }
        });
    } else if (selectedValue == 0) { // Code for "All Onboardings" selection
      // service call
      this.dashboardService.getAllOnboardingsData(selectedValue)
        .subscribe((response: any) => {

          // Getting all onboardings
          for (var i = 0; i < response.length; i++) {
            tempProjectData.push.apply(tempProjectData, response[i].projects);
          }

          // Using MatTableDataSource table functions of filter, sort & pagination
          this.onboardingsData = new MatTableDataSource(tempProjectData);

          if (this.onboardingsData != null && this.onboardingsData.data.length == 0) {
            this.noDataToDisplay = true;
            this.showNotificationError('No data found for selected CIO');
          }

          // Holding the entire list of projects in separate list variable
          if (this.onboardingsData != null) {
            this.totalOnboardingsToHold = this.onboardingsData.data;
          }
        });
    } else if (selectedValue == -1) { // Code for "Others" selection
    // service call
    this.dashboardService.getAllOnboardingsData(selectedValue)
      .subscribe((response: any) => {

        // Getting all onboardings
        for (var i = 0; i < response.length; i++) {
          if(response[i].category!=null && response[i].category.toLowerCase() == 'other') {
            tempProjectData.push.apply(tempProjectData, response[i].projects);
          } else {
            //do nothing & continue
          }
        }
        // Using MatTableDataSource table functions of filter, sort & pagination
        this.onboardingsData = new MatTableDataSource(tempProjectData);

        if (this.onboardingsData != null && this.onboardingsData.data.length == 0) {
          this.noDataToDisplay = true;
          this.showNotificationError('No data found for selected CIO');
        }

        // Holding the entire list of projects in separate list variable
        if (this.onboardingsData != null) {
          this.totalOnboardingsToHold = this.onboardingsData.data;
        }
      });
  } else {
      this.showNotificationError('Please select the CIO');
    }
  }

  // Apply search filter on the chart tabular data
  applyFilter(filterValue: string) {
    this.onboardingsData.filter = filterValue.trim().toLowerCase();

    if (this.onboardingsData.paginator) {
      this.onboardingsData.paginator.firstPage();
    }
    if (this.totalOnboardingsToHold.paginator) {
      this.totalOnboardingsToHold.paginator.firstPage();
    }
  }

  // Resetting the chart tabular data
  resetTableData() {
    this.onboardingsData.filter = null;
    this.filterChar = "";

    this.onboardingsData = new MatTableDataSource(this.totalOnboardingsToHold);

    // this.table.dataSource = this.onboardingsData;
    // this.table.renderRows();
  }

  /** Service call to get the CIO tollgate data and plot the graph */
  getCIOData(selectedValue: number) {
    // Updating the default selection
    this.selectedValue = selectedValue;

    // Resetting the required component variables before page render on CIO selection
    this.resetComponentVariables();

    if (selectedValue != null) {
      // Service call to get CIO tollgate data & plotting the data
      this.getTollgateChartData(selectedValue);

      // Service call to get CIO project details for tabular format
      this.getOnboardingsData(selectedValue);

      //Making table visible on the view
      this.showMoreDetails = true;
    }
  }

  resetComponentVariables() {
    // Resetting table data source
    this.onboardingsData = [];

    // Resetting the display flags
    this.isDataPresent = false;
    this.showMoreDetails = false;
    this.noDataToDisplay = false;

    // Resetting onboarding counts
    this.riskOnboardings = 0;
    this.totalOnboardings = 0;
    this.completedOnboardings = 0;
    this.inProgressOnboardings = 0;

    // Resetting the filter data
    this.onboardingsData.filter = null;
    this.filterChar = "";
  }

  public delayedObject = {
    color: "red",
    fontWeight: "bold"
  };
  public successObject = {
    color: "green",
    fontWeight: "bold"
  };

  /**
  * show notification error
  */
  showNotificationError(message: string): void {
    this.notificationsService.info('Information!', message, {
      timeOut: 3000,
      showProgressBar: true,
      pauseOnHover: true,
      clickToClose: true
    });
  }

  /** Plot the stacked bar chart showcasing CIO level project status */
  plotStackedGraph() {
    this.zone.runOutsideAngular(() => {
      let compCtrl = this;

      // Create chart instance
      let chart = am4core.create("chartdiv", am4charts.XYChart);
      chart.data = this.stackChartData;

      chart.fontSize = "14px";
      chart.fontFamily = "Lato, Helvetica Neue, Helvetica, Arial, sans-serif";

      // Create axes
      let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "TG";
      categoryAxis.renderer.grid.template.location = 0;
      // categoryAxis.renderer.grid.template.opacity = 0;
      categoryAxis.title.text = "(Tollgates)";

      let label = categoryAxis.renderer.labels.template;
      label.wrap = true;
      label.maxWidth = 120;

      let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.min = 0;
      valueAxis.title.text = "Projects Count";
      // valueAxis.renderer.grid.template.opacity = 0;

      function createSeries(field: string, name: string) {
        // Set up series
        let series = chart.series.push(new am4charts.ColumnSeries());
        series.name = name;
        series.dataFields.valueY = field;
        series.dataFields.categoryX = "TG";
        series.sequencedInterpolation = true;

        // Make it stacked
        series.stacked = true;
        series.columns.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;

        // Setting custom colors based on status name
        if (name == "Risk") {
          series.columns.template.fill = am4core.color("#E53935");
          series.columns.template.stroke = am4core.color("#E53935");
        } else if (name == "Completed") {
          series.columns.template.fill = am4core.color("#278D27");
          series.columns.template.stroke = am4core.color("#278D27");
        } else if (name == "InProgress") {
          series.columns.template.fill = am4core.color("#F97D09");
          series.columns.template.stroke = am4core.color("#F97D09");
        } else if (name == "NotStarted") {
          series.columns.template.fill = am4core.color("#c4a6e0");
          series.columns.template.stroke = am4core.color("#c4a6e0");
        }
        // Configure columns
        series.columns.template.width = am4core.percent(70);
        series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

        let labelBullet = series.bullets.push(new am4charts.LabelBullet());
        labelBullet.label.text = "{valueY}";
        labelBullet.locationY = 0.5;

        // method called on the click of chart
        series.columns.template.events.on('hit', hitCallback, this);

        function hitCallback(ev) {
          let tempData: any[];
          let projCount: number;
          var tbDataSource = [];
          var categoryX = ev.target.dataItem.categories.categoryX;

          compCtrl.zone.run(() => {
            // Using ".data" because dataSource of the table is created using MatTableDataSource
            tempData = compCtrl.totalOnboardingsToHold;
            projCount = compCtrl.totalOnboardingsToHold.length;
            for (var i = 0; i < projCount; i++) {
              if (categoryX == tempData[i].tollgateLevel + ": " + tempData[i].tollgateName) {
                tbDataSource.push(tempData[i]);
              }
            }
            if (tbDataSource.length == 0) {
              compCtrl.noDataToDisplay = true;
            } else {
              compCtrl.noDataToDisplay = false;
            }
            // compCtrl.table.dataSource = tbDataSource;
            // compCtrl.table.renderRows();
            compCtrl.onboardingsData = new MatTableDataSource(tbDataSource);
          });
        }
        return series;        
      }
      // Create Series
      createSeries("Risk", "Risk");
      createSeries("InProgress", "InProgress");
      createSeries("Completed", "Completed");
      chart.scrollbarX = new am4core.Scrollbar();
    });
    // Resetting the data after every selected CIO
    this.stackChartData = [];
  }

  /** Plot the PIE charts based on input data, HTML div, title and colorset */
  plotPieChart(statusPieChartData: any[], chartdiv: string, titleStr: string, colorList: any) {
    this.zone.runOutsideAngular(() => {
      let compCtrl = this;

      var container = am4core.create(chartdiv, am4core.Container);
      container.layout = "grid";
      container.fixedWidthGrid = false;
      container.width = am4core.percent(100);
      container.height = am4core.percent(100);

      function createPie(data: any, prjStatusColorList: any) {
        var chart = container.createChild(am4charts.PieChart);

        chart.width = am4core.percent(90);
        chart.height = 170;
        chart.padding(20, 0, 2, 0);

        chart.data = data;
        let title = chart.titles.create();
        title.text = titleStr;

        // Add and configure Series
        var pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "count";
        pieSeries.dataFields.category = "name";
        pieSeries.labels.template.disabled = true;
        pieSeries.ticks.template.disabled = true;
        /* pieSeries.slices.template.fill = color; */
        pieSeries.slices.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;

        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.colors.list = prjStatusColorList;

        // method called on the click of chart
        pieSeries.slices.template.events.on('hit', hitCallback, this);

        function hitCallback(ev) {
          var key: string;
          let tempData: any[];
          let projCount: number;
          var tbDataSource = [];
          var categoryX = ev.target.dataItem.category;
  
          
          compCtrl.zone.run(() => {
            // Using ".data" because dataSource of the table is created using MatTableDataSource
            tempData = compCtrl.totalOnboardingsToHold;
            projCount = compCtrl.totalOnboardingsToHold.length;
  
            if(categoryX=="InProgress" || categoryX=="Completed" || categoryX=="Risk") {
              for (var i = 0; i < projCount; i++) {
                if (categoryX.toLowerCase() == tempData[i].ktStatus.toLowerCase()) {
                  tbDataSource.push(tempData[i]);
                }
              }
            } else { //categoryX is of the format TG2 (Documentation)
              for (var i = 0; i < projCount; i++) {
                if (categoryX.toLowerCase() == (tempData[i].tollgateLevel+" ("+tempData[i].tollgateName+")").toLowerCase()) {
                  tbDataSource.push(tempData[i]);
                }
              }
            }
           
            if (tbDataSource.length == 0) {
              compCtrl.noDataToDisplay = true;
            } else {
              compCtrl.noDataToDisplay = false;
            }
            // this.tableAll.dataSource = tbDataSource;
            // this.tableAll.renderRows();
            compCtrl.onboardingsData = new MatTableDataSource(tbDataSource);
          });
        }

        return chart;
      }
      createPie(statusPieChartData, colorList);
    });
  }
}