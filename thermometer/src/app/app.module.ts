// angular module
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// utility module
import { MaterialModule } from './common/util/materialModule';
import { NgprimeModule } from './common/util/ngprimeModule';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { PanelModule } from 'primeng/panel';
import { TabViewModule } from 'primeng/primeng';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

// Export table to excel
import { MatTableExporterModule } from '../../node_modules/mat-table-exporter';

// angular components
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ConfigurationProvider } from './common/config/configuration.provider';

// common components
import { HeaderComponent } from './common/elements/header/header.component';
import { FooterComponent } from './common/elements/footer/footer.component';
import { NavbarComponent } from './common/elements/navbar/navbar.component';
import { ModalComponent } from './common/elements/modal/modal.component';

// page components
import { HomePageComponent } from './home-page/home-page.component';
import { ViewOnboardingComponent } from './view-onboarding/view-onboarding.component';
import { NewOnboardingComponent } from './new-onboarding/new-onboarding.component';
import { DashboardComponent } from './dashboard/dashboard.component';

// resolvers
import { CIOListResolver } from './common/resolvers/cioList.resolver';
import { ViewOnBoardingListResolver } from './common/resolvers/viewOnBoardingList.resolver';
import { ViewOnBoardingDetailsResolver } from './common/resolvers/viewOnBoardingDetails.resolver';
import { CreateOnBoardingDetailsResolver } from './common/resolvers/createOnBoardingDetails.resolver';

// NG module
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent,
    ModalComponent,
    ViewOnboardingComponent,
    NewOnboardingComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    NgprimeModule,
    SimpleNotificationsModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    PanelModule,
    TabViewModule,
    NgbModule,
    MatTableExporterModule

  ],
  providers: [
    Ng4LoadingSpinnerService,
    ConfigurationProvider,
    CIOListResolver,
    ViewOnBoardingListResolver,
    ViewOnBoardingDetailsResolver,
    CreateOnBoardingDetailsResolver
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent]
})

export class AppModule { }
