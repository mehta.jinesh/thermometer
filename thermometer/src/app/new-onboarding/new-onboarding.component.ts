// angular
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { MatDialog } from '@angular/material';

// utility
import { cloneDeep } from 'lodash-es';
import { NotificationsService } from 'angular2-notifications';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

// injections
import { OnboardingService } from '../common/services/onboarding.service';
import { ModalComponent } from '../common/elements/modal/modal.component';

// interfaces
import { TollGate, CriteriaQuestionDetail, QuestionDetail } from '../common/types/tollgate';

// constants
import { CIO } from '../common/types/cio';
import { RISK_LEVELS } from '../common/constants/risk.constants';
import { CRITICAL_LEVELS } from '../common/constants/criticality.constants';

@Component({
  selector: 'app-new-onboarding',
  templateUrl: './new-onboarding.component.html',
  styleUrls: ['./new-onboarding.component.scss']
})
export class NewOnboardingComponent implements OnInit {

  @ViewChild('stepper') stepper: MatStepper;

  isLinear = false;
  isViewOnBoarding = false;

  onBoardingForm: FormGroup;
  groupForm: FormGroup[] = [];
  formData: any = [];
  projectId: any;
  metaData: TollGate[];
  onBoardingFormProgress: number;
  thermoPercentage: any;
  params: any;
  projectData: any;
  cioId: any;
  cioList: CIO[];
  riskLevels: string[];
  criticality: string;
  criticalityLevels: any[];

  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private spinnerService: Ng4LoadingSpinnerService,
    private notificationsService: NotificationsService,
    private onboardingService: OnboardingService) { }

  ngOnInit() {
    this.initializeComponent();
  }

  /**
   * intialize component
   */
  initializeComponent(): void {

    // risk levels
    this.riskLevels = RISK_LEVELS;

    // criticality levels
    this.criticalityLevels = CRITICAL_LEVELS;

    // cio's list
    this.cioList = this.route.snapshot.data.cioList;

    // subscription route param service
    this.route.params.subscribe((params: Params) => {
      // project ID
      this.projectId = params.id ? params.id : null;

      if (this.projectId) {
        this.isViewOnBoarding = true;
        this.viewOnBoarding();
      } else {
        this.isViewOnBoarding = false;
        this.createOnBoarding();
      }

      this.spinnerService.hide();
    });
  }

  // ------------------------------- CREATE ON-BOARDING ------------------------------- //
  /**
   * create On Boarding
   */
  createOnBoarding(): void {
    // create on boarding details
    this.metaData = this.route.snapshot.data.createOnBoardingDetails.tollGateModel;
    this.updateThermoPercentage();
    this.initializeFormGroup();
    this.buildFormData();
  }

  /**
   * setting thermo percentage
   */
  updateThermoPercentage(): void {
    this.metaData.forEach((tg: TollGate, index) => {
      tg.criteriaQuestionDetails.forEach((cqd: CriteriaQuestionDetail) => {
        cqd.ktThermometerPercentage = 0;
      });
    });
  }

  /**
   * intialize form group
   */
  initializeFormGroup(): void {

    this.onBoardingForm = this.formBuilder.group({
      cioId: [null, Validators.required],
      criticality: [null, Validators.required],
      plannedStartDate: [null, Validators.required],
      completionDate: [null, Validators.required],
      actualStartDate: [null, Validators.required],
      applicationName: [null, Validators.required],
      geItLeader: [null, Validators.required],
      geProjectOwner: [null, Validators.required],
      // geFunctionalLeader: [null, Validators.required],
      pitcher: [null, Validators.required],
      // tcsProjectManager: [null, Validators.required],
      catcher: [null, Validators.required],
      ktOwner: [null, Validators.required],
      // subBusiness: [null, Validators.required],
      // subBusinessLead: [null, Validators.required],
      exportControl: [null, Validators.required],
      monitoring: [false, Validators.required],
      functionalSupport: [false, Validators.required],
      functionalMonitor: [false, Validators.required],
      onsiteSupport: [false, Validators.required],
      admin: [false, Validators.required],
      enhancement: [false, Validators.required]
    });

    // Form Progress
    this.onBoardingFormProgress = this.calculateFormProgress(this.onBoardingForm);
    this.onBoardingForm.valueChanges.subscribe(val => {
      this.onBoardingFormProgress = this.calculateFormProgress(this.onBoardingForm);
    });
  }

  /**
   * build form data to sync between ui and data
   */
  buildFormData(): void {
    const modifiedMetaData = cloneDeep(this.metaData);

    modifiedMetaData.forEach((tg: TollGate, index) => {

      const tempData: any = {};

      // Form Group
      this.groupForm[index] = new FormGroup({});

      tg.criteriaQuestionDetails.forEach((cqd: CriteriaQuestionDetail) => {
        cqd.questionDetails.forEach((qd: QuestionDetail) => {
          // Form Control: Radio
          this.groupForm[index].addControl('form-radio-control-cqd-' + cqd.id + '-qd-' + qd.id,
            new FormControl(null, [Validators.required]));
          // Form Control: Input
          this.groupForm[index].addControl('form-input-control-cqd-' + cqd.id + '-qd-' + qd.id,
            new FormControl(null, []));
        });
      });

      this.groupForm[index].addControl('riskDesc-' + tg.id,
        new FormControl(tg.riskDesc, [Validators.required]));

      this.groupForm[index].addControl('riskLevel-' + tg.id,
        new FormControl(tg.risk, [Validators.required]));

      this.groupForm[index].addControl('mitigation-' + tg.id,
        new FormControl(tg.mitigation, []));

      this.groupForm[index].addControl('status-' + tg.id,
        new FormControl(tg.status, []));

      tempData.form = this.groupForm[index];
      tempData.tollgate = tg;

      // Dynamic Validation
      // if risk level is NONE, then remove required validation from risk description
      this.groupForm[index].get(`riskLevel-${tg.id}`).valueChanges.subscribe(level => {
        if (!!level && level === 'NONE') {
          this.groupForm[index].get(`riskDesc-${tg.id}`).setValidators(null);
        } else {
          this.groupForm[index].get(`riskDesc-${tg.id}`).setValidators([Validators.required]);
        }
        this.groupForm[index].get(`riskDesc-${tg.id}`).updateValueAndValidity();
      });

      // Form Progress
      tempData.progress = this.calculateFormProgress(tempData.form);
      tempData.form.valueChanges.subscribe(val => {
        tempData.progress = this.calculateFormProgress(tempData.form);
      });

      this.formData.push(tempData);
    });
  }

  // ------------------------------- VIEW ON-BOARDING ------------------------------- //
  /**
   * view On Boarding
   */
  viewOnBoarding(): void {
    // view on boarding details
    this.projectData = this.route.snapshot.data.viewOnBoardingDetails;
    this.initializeFormGroupById();
    this.buildFormDataById();
  }

  /**
   * intialize form group by id
   */
  initializeFormGroupById(): void {

    const boardingFormData = cloneDeep(this.projectData);

    this.onBoardingForm = this.formBuilder.group({
      cioId: [boardingFormData.cio.id, Validators.required],
      criticality: [boardingFormData.criticality, Validators.required],
      plannedStartDate: [boardingFormData.plannedStartDate, Validators.required],
      completionDate: [boardingFormData.completionDate, Validators.required],
      actualStartDate: [boardingFormData.actualStartDate, Validators.required],
      applicationName: [boardingFormData.applicationName, Validators.required],
      geItLeader: [boardingFormData.geItLeader, Validators.required],
      geProjectOwner: [boardingFormData.geProjectOwner, Validators.required],
      // geFunctionalLeader: [boardingFormData.geFunctionalLeader, Validators.required],
      pitcher: [boardingFormData.pitcher, Validators.required],
      // tcsProjectManager: [boardingFormData.tcsProjectManager, Validators.required],
      catcher: [boardingFormData.catcher, Validators.required],
      ktOwner: [boardingFormData.ktOwner, Validators.required],
      // subBusiness: [boardingFormData.subBusiness, Validators.required],
      // subBusinessLead: [boardingFormData.subBusinessLead, Validators.required],
      exportControl: [boardingFormData.exportControl, Validators.required],
      monitoring: [boardingFormData.monitoring, Validators.required],
      functionalSupport: [boardingFormData.functionalSupport, Validators.required],
      functionalMonitor: [boardingFormData.functionalMonitor, Validators.required],
      onsiteSupport: [boardingFormData.onsiteSupport, Validators.required],
      admin: [boardingFormData.admin, Validators.required],
      enhancement: [boardingFormData.enhancement, Validators.required]
    });

    // selected cio
    this.cioId = boardingFormData.cio.id;
    // selected criticality
    this.criticality = boardingFormData.criticality;

    // Form Progress
    this.onBoardingFormProgress = this.calculateFormProgress(this.onBoardingForm);
    this.onBoardingForm.valueChanges.subscribe(val => {
      this.onBoardingFormProgress = this.calculateFormProgress(this.onBoardingForm);
    });
  }

  /**
   * build form data to sync between ui and data
   */
  buildFormDataById(): void {
    const modifiedProjectData = cloneDeep(this.projectData);

    // cast object to array
    modifiedProjectData.tollGateModel = Object.values(modifiedProjectData.tollGateModel); // [TWEAK]
    modifiedProjectData.tollGateModel.forEach((tg: any, index) => {

      // tollgate in progress index
      if (tg.status === 'INPROGRESS') {
        this.stepper.selectedIndex = index + 1;
      }

      const tempData: any = {};

      // Form Group
      this.groupForm[index] = new FormGroup({});

      // cast object to array
      tg.criteriaQuestionDetails = Object.values(tg.criteriaQuestionDetails); // [TWEAK]
      tg.criteriaQuestionDetails.forEach((mc: any) => {
        mc.ktThermometerPercentage = mc.thermoPercentage;
        mc.questionDetails.forEach((qd: any) => {
          // Form Control: Radio
          this.groupForm[index].addControl('form-radio-control-cqd-' + mc.id + '-qd-' + qd.id,
            new FormControl(qd.value, [Validators.required]));
          // Form Control: Input
          this.groupForm[index].addControl('form-input-control-cqd-' + mc.id + '-qd-' + qd.id,
            new FormControl(qd.remarks, []));
        });
      });

      // NULL -> NONE [TWEAK]
      tg.risk = !tg.risk ? 'NONE' : tg.risk;

      this.groupForm[index].addControl('riskDesc-' + tg.id,
        new FormControl(tg.riskDesc, [Validators.required]));
      this.groupForm[index].addControl('riskLevel-' + tg.id,
        new FormControl(tg.risk, [Validators.required]));
      this.groupForm[index].addControl('mitigation-' + tg.id,
        new FormControl(tg.mitigation, []));
      this.groupForm[index].addControl('status-' + tg.id,
        new FormControl(tg.status, []));

      tempData.form = this.groupForm[index];
      tempData.tollgate = tg;

      // Dynamic Validation
      // if risk level is NONE, then remove required validation from risk description
      this.groupForm[index].get(`riskLevel-${tg.id}`).valueChanges.subscribe(level => {
        if (!!level && level === 'NONE') {
          this.groupForm[index].get(`riskDesc-${tg.id}`).setValidators(null);
        } else {
          this.groupForm[index].get(`riskDesc-${tg.id}`).setValidators([Validators.required]);
        }
        this.groupForm[index].get(`riskDesc-${tg.id}`).updateValueAndValidity();
      });

      // Form Progress
      tempData.progress = this.calculateFormProgress(tempData.form);
      tempData.form.valueChanges.subscribe(val => {
        tempData.progress = this.calculateFormProgress(tempData.form);
      });

      this.formData.push(tempData);
    });
  }

  // --------------------------- TEMPLATE ACTIONS --------------------------------------- //

  /**
   * submitting on boarding form
   */
  onSubmitOnboardingForm(): void {
    if (!this.onBoardingForm.invalid) {
      this.onboardingService.addOnBoardingDetails(this.onBoardingForm.value)
        .subscribe((response: any) => {
          console.log(response);
          this.projectId = response.id;
          // show set of messages based on response
          this.showNotificationSuccess('On Boarding Step ' + response.message);
          this.stepper.next();
        });
    } else {
      this.showNotificationError('Please fill mandatory details.');
    }
  }

  /**
   * submitting tollgates form
   */
  onSubmitTollgateForm(form: FormGroup, tollgate: TollGate): void {

    const tolGateDetails = [];

    // requested object for submit tollagte
    const reqObj = {
      projectId: this.projectId,
      tollGateId: tollgate.id,
      criteriaPercentage: [],
      risk: null,
      riskDesc: null,
      mitigation: null,
      status: tollgate.status,
      saveTollGateDetails: []
    };

    tollgate.criteriaQuestionDetails.forEach((cqd: CriteriaQuestionDetail) => {

      // criteria object
      const criteriaInfo = {
        criteriaId: cqd.id,
        percentage: cqd.ktThermometerPercentage
      };
      reqObj.criteriaPercentage.push(criteriaInfo);

      cqd.questionDetails.forEach((qd) => {

        // question object
        const tolgateObj = {
          actions: 'action',
          value: false,
          remarks: 'Yes',
          questionId: qd.id
        };
        tolGateDetails.push(tolgateObj);
      });

    });

    // setting values to form control names
    for (const key in form.value) {
      if (form.value.hasOwnProperty(key) && !key.startsWith('riskDesc') && !key.startsWith('miti') && !key.startsWith('riskLevel')) {
        tolGateDetails.forEach((elem, index) => {
          if (elem.questionId.toString() === key.split('-qd-', 2)[1] && key.includes('radio')) {
            elem.value = form.value[key] === 'true' ? true : false;
          }
          if (elem.questionId.toString() === key.split('-qd-', 2)[1] && key.includes('input')) {
            elem.remarks = form.value[key];
          }
        });
      } else if (key.startsWith('riskDesc')) {
        reqObj.riskDesc = form.value[key];
      } else if (key.startsWith('riskLevel')) {
        reqObj.risk = form.value[key];
      } else if (key.startsWith('miti')) {
        reqObj.mitigation = form.value[key];
      } else if (key.startsWith('status')) {
        reqObj.status = form.value[key];
      }
    }

    // NONE -> NULL [TWEAK]
    reqObj.risk = !!reqObj.risk && reqObj.risk !== 'NONE' ? reqObj.risk : null;

    reqObj.saveTollGateDetails = tolGateDetails;

    if (!form.invalid) {
      this.onboardingService.saveTollGateDetails(reqObj)
        .subscribe(response => {
          console.log(response);
          // show set of messages based on response
          if (response.message === 'Tollgate is already completed') {
            this.showNotificationError('This particular step of Tolgate is already completed!');
          } else if (response.message === 'Tollgate is not started yet') {
            this.showNotificationError('Previous steps are yet to be completed!');
          } else {
            this.showNotificationSuccess(tollgate.tollgateLevel + ' ' + response.message);
          }
          this.stepper.next();
        })
    } else {
      this.showNotificationError('Please fill mandatory details.');
    }
  }

  /**
   * open tollgate staus box for setting status
   */
  openTollgateStatusDialog(form: FormGroup, tollgate: TollGate): void {

    if (!form.invalid) {

      // dialog box options
      const dialogRef = this.dialog.open(ModalComponent, {
        width: '300px',
        data: tollgate
      });

      // subscription to dialog box
      dialogRef.afterClosed().subscribe(status => {
        // setting up new status if defined else provide previous status
        tollgate.status = status ? status : tollgate.status;

        // when save status is not clicked
        if (!!status) {
          if (!!tollgate.status) {
            this.onSubmitTollgateForm(form, tollgate);
          }
        }
      });

    } else {
      this.showNotificationError('Please fill mandatory details.');
    }
  }

  /**
   * calculate thermo percentage
   */
  onChangeRadio(criteriaId, fd) {
    let form = fd.form;
    let tolgate = fd.tollgate;

    let c = 0;
    for (const key in form.value) {
      if (form.value.hasOwnProperty(key) && !key.startsWith('riskDesc') && !key.startsWith('riskLevel') && !key.startsWith('miti') && !key.startsWith('status')) {
        if (key.split('cqd-')[1].split('-')[0].toString() === criteriaId.toString()) {
          if (form.value[key] === "true") {
            c++;
          }
        }
      }
    }

    tolgate.criteriaQuestionDetails.forEach((cqd: CriteriaQuestionDetail) => {
      if (cqd.id.toString() === criteriaId.toString()) {
        const percentage = (c / cqd.questionDetails.length) * 100;
        cqd.ktThermometerPercentage = +percentage.toFixed(2);
      }
    });

  }

  /**
   * mat selection change
   */
  selectionChange(event): void {
    // scroll element based on selected index
    if (event.selectedIndex) {
      const element = document.querySelectorAll('.mat-vertical-stepper-header.mat-step-header')[event.selectedIndex-1];
      element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
    }
  }

  compareCIO(cioId1: any, cioId2: any): boolean {
    return cioId1 === cioId2;
  }

  compareRisk(risk1: string, risk2: string): boolean {
    return risk1 === risk2;
  }

  compareCriticality(cric1: string, cric2: string): boolean {
    return cric1 === cric2;
  }

  // --------------------------- COMMMON --------------------------------------- //

  /**
   * calculate form progess
   */
  calculateFormProgress(form: FormGroup): number {
    let totalKeys = 0;
    let validKeys = 0;

    Object.keys(form.controls).forEach(key => {
      totalKeys++;
      if (form.controls[key].valid) {
        validKeys++;
      }
    });
    return Math.floor(validKeys / totalKeys * 100);
  }

  /**
   * show notification error
   */
  showNotificationError(message): void {
    this.notificationsService.error('Error!', message, {
      timeOut: 3000,
      showProgressBar: true,
      pauseOnHover: true,
      clickToClose: true
    });
  }

  /**
   * show notification success
   */
  showNotificationSuccess(message): void {
    this.notificationsService.success('Successfully!', message, {
      timeOut: 3000,
      showProgressBar: true,
      pauseOnHover: true,
      clickToClose: true
    });
  }

}
